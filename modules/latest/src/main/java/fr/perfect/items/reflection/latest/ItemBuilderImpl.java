package fr.perfect.items.reflection.latest;

import fr.perfect.items.reflection.common.ItemBuilder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemBuilderImpl implements ItemBuilder {

	/**
	 * @param stack is the source
	 */
	@Override
	public void setUnbreakable(ItemStack stack) {
		ItemMeta meta = stack.getItemMeta();
		meta.setUnbreakable(true);
		stack.setItemMeta(meta);
	}

	/**
	 * @param stack is the source
	 * @param modelID is the customModelData
	 */
	@Override
	public void setModelID(ItemStack stack, int modelID) {
		ItemMeta meta = stack.getItemMeta();
		meta.setCustomModelData(modelID);
		System.out.println(modelID + "hehe");
		stack.setItemMeta(meta);
	}
}
