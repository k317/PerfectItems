package fr.perfect.items.reflection.latest;

import fr.perfect.items.reflection.common.ItemBuilder;
import fr.perfect.items.reflection.common.VersionManager;

public class VersionManagerImpl implements VersionManager {

	private final ItemBuilder builder = new ItemBuilderImpl();

	/**
	 * @return the item utils class instance
	 */
	@Override
	public ItemBuilder getItemBuilder() {
		return builder;
	}
}
