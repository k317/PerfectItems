package fr.perfect.serializer;

import de.tr7zw.changeme.nbtapi.NBTCompound;
import org.bukkit.Material;

import java.lang.reflect.Field;
import java.util.UUID;

public class NBTSerializer {

	/**
	 * convert a nbt compound to a complex data class with field and a non parameter constructor
	 * only work with Material, int, string, double and boolean's field because of lag
	 *
	 * @param source the data source (ur NBTItem for example)
	 * @param tclass the data class Class object
	 * @param <T> the data class type
	 */
	public static <T> T deserializeObject(NBTCompound source, Class<T> tclass) {
		try {
			T value = tclass.newInstance();
			for (Field field : tclass.getDeclaredFields()) {
				field.setAccessible(true);
				String name = field.getName();
				if (field.getType().equals(int.class)) {
					field.set(value, source.getInteger(name));
				} else if (field.getType().equals(double.class)) {
					field.set(value, source.getDouble(name));
				}  else if (field.getType().equals(boolean.class)) {
					field.set(value, source.getBoolean(name));
				} else if (field.getType().equals(String.class)) {
					field.set(value, source.getString(name));
				} else if (field.getType().equals(Material.class)) {
					field.set(value, Material.matchMaterial(source.getString(name)));
				} else {
					field.set(value, deserializeObject(source.getCompound(name), (Class)field.getType()));
				}
			}
			return value;
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}

	/**
	 * convert a nbt compound to a data class.
	 * only work with Material, int, string, double and boolean's field because of lag
	 *
	 * @param source the data source (ur NBTItem for example)
	 * @param tClass the data class Class object
	 * @param <T> the data class type
	 */
	public static <T> T deserialize(NBTCompound source, Class<T> tClass) {
		if(tClass == int.class) {
			return (T) source.getInteger("value");
		} else if(tClass == double.class) {
			return (T) source.getDouble("value");
		} else if(tClass == String.class) {
			return (T) source.getString("value");
		} else if(tClass == boolean.class) {
			return (T) source.getBoolean("value");
		} else if(tClass == Material.class) {
			return (T) Material.matchMaterial(source.getString("value"));
		} else {
			return deserializeObject(source, tClass);
		}
	}

	/**
	 * convert a data class to nbt compound.
	 * only work with Material, int, string, double and boolean because of lag
	 *
	 * @param compound the current compound (for example ur NBTItem)
	 * @param current the data class instance in question
	 */
	public static void serialize(NBTCompound compound, Object current) {
		Class<?> tclass = current.getClass();
		if(tclass == Integer.class) {
			compound.setInteger("value", (int) current);
		} else if(tclass == Double.class) {
			compound.setDouble("value", (double) current);
		} else if(tclass == String.class) {
			compound.setString("value", (String) current);
		} else if(tclass == Material.class) {
			compound.setString("value", ((Material) current).name());
		} else if(tclass == Boolean.class) {
			compound.setBoolean("value", (boolean) current);
		} else {
			serializeObject(compound, current);
		}
	}

	/**
	 * convert a complex data class with field & constructor to nbt compound.
	 * only work with Material, int, string, double and boolean because of lag
	 *
	 * @param compound the current compound (for example ur NBTItem)
	 * @param current the data class instance in question
	 */
	public static void serializeObject(NBTCompound compound, Object current) {
		try {
			Class<?> tclass = current.getClass();
			for (Field field : tclass.getDeclaredFields()) {
				field.setAccessible(true);
				Object value = field.get(current);
				String name = field.getName();
				if (field.getType().equals(int.class)) {
					compound.setInteger(name, (Integer) value);
				} else if (field.getType().equals(double.class)) {
					compound.setDouble(name, (Double) value);
				}  else if (field.getType().equals(boolean.class)) {
					compound.setBoolean(name, (Boolean) value);
				} else if (field.getType().equals(String.class)) {
					compound.setString(name, (String)value);
				} else if (field.getType().equals(UUID.class)) {
					compound.setUUID(name, (UUID)value);
				} else if(field.getType().equals(Material.class)) {
					compound.setString(name, ((Material) value).name());
				} else {
					NBTCompound comp = compound.addCompound(name);
					serializeObject(comp, current);
				}
			}
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}
}
