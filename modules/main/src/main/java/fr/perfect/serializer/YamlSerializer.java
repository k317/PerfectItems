package fr.perfect.serializer;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;

import java.lang.reflect.Field;

public class YamlSerializer {

	/**
	 * convert a yaml section to a data class.
	 * only work with Material, int, string, double and boolean's field because of lag
	 *
	 * @param section the data source (ur NBTItem for example)
	 * @param tClass the data class Class object
	 * @param <T> the data class type
	 */
	public static <T> T deserialize(ConfigurationSection section, Class<T> tClass) {
		if(tClass == int.class) {
			return (T) (Integer) section.getInt("value");
		} else if(tClass == double.class) {
			return (T) (Double) section.getDouble("value");
		} else if(tClass == String.class) {
			return (T) section.getString("value");
		} else if(tClass == boolean.class) {
			return (T) (Boolean) section.getBoolean("value");
		} else if(tClass == Material.class) {
			return (T) Material.matchMaterial(section.getString("value"));
		} else {
			return deserializeObject(section, tClass);
		}
	}

	/**
	 * convert a yaml section to a complex data class with field and a non parameter constructor
	 * only work with Material, int, string, double and boolean's field because of lag
	 *
	 * @param source the data source
	 * @param tClass the data class Class object
	 * @param <T> the data class type
	 */
	public static <T> T deserializeObject(ConfigurationSection source, Class<T> tClass) {
		try {
			T value = tClass.newInstance();
			if(source == null)return value;
			for(Field field : tClass.getDeclaredFields()) {
				field.setAccessible(true);
				String name = field.getName().toLowerCase();
				if(field.getType().equals(int.class)) {
					field.set(value, source.getInt(name));
				} else if(field.getType().equals(double.class)) {
					field.set(value, source.getDouble(name));
				} else if(field.getType().equals(boolean.class)) {
					field.set(value, source.getBoolean(name));
				} else if(field.getType().equals(String.class)) {
					field.set(value, source.getString(name));
				} else if(field.getType().equals(Material.class)) {
					field.set(value, Material.matchMaterial(source.getString(name)));
				} else {
					field.set(value, deserializeObject(source.getConfigurationSection(name), (Class<T>) field.getType()));
				}
			}
			return value;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * convert a data class to yaml section.
	 * only work with Material, int, string, double and boolean because of lag
	 *
	 * @param section the current section
	 * @param current the data class instance in question
	 */
	public static void serialize(ConfigurationSection section, Object current) {
		Class<?> tclass = current.getClass();
		if(tclass == Integer.class) {
			section.set("value", (int) current);
		} else if(tclass == Double.class) {
			section.set("value", (double) current);
		} else if(tclass == String.class) {
			section.set("value", (String) current);
		} else if(tclass == Material.class) {
			section.set("value", ((Material) current).name());
		} else if(tclass == Boolean.class) {
			section.set("value", (boolean) current);
		} else {
			serializeObject(section, current);
		}
	}

	/**
	 * convert a complex data class with field & constructor to yaml section.
	 * only work with Material, int, string, double and boolean because of lag
	 *
	 * @param compound the current section
	 * @param current the data class instance in question
	 */
	public static void serializeObject(ConfigurationSection compound, Object current) {
		try {
			Class<?> tclass = current.getClass();
			for(Field field : tclass.getDeclaredFields()) {
				field.setAccessible(true);
				Object value = field.get(current);
				String name = field.getName();
				if(field.getType().equals(int.class)) {
					compound.set(name, (int) value);
				} else if(field.getType().equals(double.class)) {
					compound.set(name, (double) value);
				} else if(field.getType().equals(boolean.class)) {
					compound.set(name, (boolean) value);
				} else if(field.getType().equals(String.class)) {
					compound.set(name, (String) value);
				} else if(field.getType().equals(Material.class)) {
					compound.set(name, ((Material) value).name());
				} else {
					ConfigurationSection section = compound.createSection(name);
					serialize(section, current);
				}
			}
		} catch(Exception e) {
			throw new RuntimeException();
		}
	}
}
