package fr.perfect.reloader;

public interface Reloadable {

	/**
	 * Function invoked each time that u want to reload
	 */
	void load();
}
