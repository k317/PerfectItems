package fr.perfect.items.wrapper;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface BuildWrapper extends Wrapper {

	/**
	 * @param location is where the block will be / was
	 * @param player is the source
	 * @return if yes or no this plugin authorize him to build
	 */
	boolean canBuild(Location location, Player player);

}
