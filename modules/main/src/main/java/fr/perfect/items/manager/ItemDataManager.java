package fr.perfect.items.manager;

import de.tr7zw.changeme.nbtapi.NBTCompound;
import de.tr7zw.changeme.nbtapi.NBTItem;
import fr.perfect.items.model.ItemProperty;
import fr.perfect.serializer.NBTSerializer;

import javax.inject.Singleton;

@Singleton
public class ItemDataManager {

	/**
	 * @param stack is the target
	 * @param property is what u want to get inside the item
	 * @return the property data associated, or null if no data =
	 * @param <T> property data type class associated
	 */
	public <T> T getItemProperty(NBTItem stack, ItemProperty property) {
		NBTCompound compound = stack.getOrCreateCompound(property.name());
		if (property.getTclass() == null)return null;
		return NBTSerializer.deserialize(compound, (Class<T>) property.getTclass());
	}

	/**
	 * @param stack is the target
	 * @param property is the property that u want to check
	 * @return if yes or no the item have this property
	 */
	public boolean hasItemProperty(NBTItem stack, ItemProperty property) {
		return stack.hasTag(property.name());
	}

	/**
	 * @param stack is the target
	 * @param property is the property type that u want to store
	 * @param t is the data class associated
	 * @param <T> is the data class type
	 */
	public <T> void setItemProperty(NBTItem stack, ItemProperty property, T t) {
		NBTCompound compound = stack.getOrCreateCompound(property.name());
		if(t == null)return;
		NBTSerializer.serialize(compound, t);
	}
}
