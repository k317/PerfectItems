package fr.perfect.items.model;

import fr.perfect.items.data.FarmHoeData;
import fr.perfect.items.data.FarmSwordData;
import org.bukkit.Material;

public enum ItemProperty {

	FARMHOE(FarmHoeData.class),
	HAMMER(int.class),
	KEEPINVENTORY(null),
	FARMSWORD(FarmSwordData.class),
	SELLSTICK(double.class),
	LINEBUILDER(Material.class),
	CHESTVIEWER(null),
	CROWBAR(null),
	INVSEE(null),

	DURABILITY(int.class),
	OWNERNAME(String.class),
	STATISTIC(int.class)
	;

	private final Class<?> tclass;

	ItemProperty(Class<?> tclass) {
		this.tclass = tclass;
	}

	public Class<?> getTclass() {
		return tclass;
	}
}
