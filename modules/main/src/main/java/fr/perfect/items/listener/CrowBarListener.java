package fr.perfect.items.listener;

import de.tr7zw.changeme.nbtapi.NBTItem;
import fr.perfect.items.manager.ItemDataManager;
import fr.perfect.items.manager.UsureManager;
import fr.perfect.items.manager.WrapperManager;
import fr.perfect.items.model.ItemProperty;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Door;

import javax.inject.Inject;

public class CrowBarListener implements Listener {

	private final ItemDataManager manager;
	private final UsureManager usure;
	private final WrapperManager wrapper;

	@Inject
	public CrowBarListener(ItemDataManager itemData, UsureManager usure, WrapperManager wrapper) {
		this.manager = itemData;
		this.usure = usure;
		this.wrapper = wrapper;
	}

	@EventHandler
	public void onBlockClick(PlayerInteractEvent event) {

		if(event.hasItem()) return;
		if(event.getAction() != Action.RIGHT_CLICK_BLOCK) return;

		Player player = event.getPlayer();
		ItemStack stack = player.getItemInHand();

		if(stack == null) return;

		NBTItem nbtItem = new NBTItem(stack);
		Block block = event.getClickedBlock();

		if(!manager.hasItemProperty(nbtItem, ItemProperty.CHESTVIEWER)) return;
		if(!(block.getState().getData() instanceof Door))return;
		if(!wrapper.canBuild(player, block.getLocation()))return;

		Door door = (Door) block.getState().getData();

		door.setOpen(true);

		event.setCancelled(true);
		usure.use(stack, nbtItem, player, 1);
		nbtItem.applyNBT(stack);
	}

}
