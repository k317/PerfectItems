package fr.perfect.items.listener;

import de.tr7zw.changeme.nbtapi.NBTItem;
import fr.perfect.items.data.FarmHoeData;
import fr.perfect.items.manager.ItemDataManager;
import fr.perfect.items.manager.PricesManager;
import fr.perfect.items.manager.UsureManager;
import fr.perfect.items.manager.WrapperManager;
import fr.perfect.items.model.ItemProperty;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class HoeListener implements Listener {

	private final PricesManager prices;
	private final ItemDataManager manager;
	private final UsureManager usure;
	private final WrapperManager wrapper;

	@Inject
	public HoeListener(ItemDataManager manager, PricesManager prices, UsureManager usure, WrapperManager wrapper) {
		this.manager = manager;
		this.prices = prices;
		this.usure = usure;
		this.wrapper = wrapper;
	}

	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();

		ItemStack stack = player.getItemInHand();

		if(stack == null)return;
		if(stack.getType() == Material.AIR)return;

		NBTItem nbtItem = new NBTItem(stack);

		if(!manager.hasItemProperty(nbtItem, ItemProperty.FARMHOE)) return;
		if(!wrapper.canBuild(player, block.getLocation()))return;

		List<ItemStack> loots = new ArrayList<>();
		FarmHoeData data = manager.getItemProperty(nbtItem, ItemProperty.FARMHOE);

		event.setCancelled(true);

		int radius = (data.getRadius()-1)/2;

		for(int x = -radius; x <= radius; x++) {
			for(int z = -radius; z <= radius; z++) {
				Block current = block.getLocation().clone().add(x, 0, z).getBlock();
				if(current.getData() >= getRequiredMeta(block)) {
					loots.addAll(current.getDrops());
					Material type = current.getType();
					current.setType(Material.AIR);
					current.setType(type);
				}
			}
		}

		if(data.isSell()) {
			int money = 0;
			for(ItemStack loot : loots) {
				System.out.println(prices.toString());
				Double price = prices.getPrices().get(loot.getType());
				if(price == null) continue;
				money += data.getMultiplier()*price;
			}
			player.sendMessage("§7Tu as reçu " + money + "$");
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "/eco give " + player.getName() + " " + money);
		} else {
			for(ItemStack loot : loots) {
				player.getInventory().addItem(loot);
			}
			if(player.getInventory().firstEmpty() == -1) player.sendTitle("§c!", "§cInventaire Plein");
		}
		usure.use(stack, nbtItem, player, 1);
		nbtItem.applyNBT(stack);
	}

	private int getRequiredMeta(Block block) {
		Material type = block.getType();
		return  (type == Material.STRING || type == Material.COCOA) ? 8
		: (type == Material.MELON || type == Material.PUMPKIN) ? 0
		: (type == Material.NETHER_WARTS) ? 3
		: 7;
	}
}
