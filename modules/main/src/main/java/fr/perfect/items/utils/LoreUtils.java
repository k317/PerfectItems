package fr.perfect.items.utils;

import de.tr7zw.changeme.nbtapi.NBTItem;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class LoreUtils {

	private static final String LORE_KEY = "itemdefaultlore";

	/**
	 * store a "lore model" in nbt data
	 *
	 * @param item is the target
	 * @param lore is the lore
	 */
	public static void storeLore(NBTItem item, List<String> lore) {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		try {
			for (String element : lore) {
				out.writeUTF(element);
			}
		}catch(IOException e) {
			throw new RuntimeException(e);
		}
		item.setByteArray(LORE_KEY, baos.toByteArray());
	}

	/**
	 * store a "lore model" in nbt data
	 *
	 * @param item is the source
	 * @return the lore stored
	 */
	public static List<String> readLore(NBTItem item) {
		List<String> result = new ArrayList<>();
		ByteArrayInputStream bais = new ByteArrayInputStream(item.getByteArray(LORE_KEY));
		DataInputStream in = new DataInputStream(bais);
		try {
			while (in.available() > 0) {
				String element = in.readUTF();
				result.add(element);
			}
		}catch(Exception e) {
			throw new RuntimeException(e);
		}
		return result;
	}
}
