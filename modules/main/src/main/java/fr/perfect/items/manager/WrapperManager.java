package fr.perfect.items.manager;

import fr.perfect.items.wrapper.BuildWrapper;
import fr.perfect.items.wrapper.Wrapper;
import fr.perfect.items.wrapper.impl.SuperiorSkyblockWrapper;
import fr.perfect.reloader.Reloadable;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Singleton
public class WrapperManager implements Reloadable {

	private final List<Wrapper> wrappers = new ArrayList<>();
	private final List<BuildWrapper> buildWrappers = new ArrayList<>();
	private final JavaPlugin plugin;

	@Inject
	public WrapperManager(JavaPlugin plugin) {
		this.plugin = plugin;
	}

	/**
	 * @param player is the source
	 * @param location is where the block will be / was
	 * @return is yes or no he can build here
	 */
	public boolean canBuild(Player player, Location location) {
		for(BuildWrapper wrapper : buildWrappers)
			if(!wrapper.canBuild(location, player))return false;
		return true;
	}

	/**
	 * Function invoked each time that u want to reload
	 */
	@Override
	public void load() {
		ConfigurationSection section = plugin.getConfig().getConfigurationSection("wrapper");
		for(String key : section.getKeys(false)) {
			if(!section.getBoolean(key))continue;
			Wrapper wrapper = null;
			try {
				wrapper = WrapperType.valueOf(key.toUpperCase(Locale.ROOT)).wclass.newInstance();
			}catch(InstantiationException | IllegalAccessException e) {
				throw new RuntimeException(e);
			}
			if(wrapper instanceof BuildWrapper)buildWrappers.add((BuildWrapper) wrapper);
			wrappers.add(wrapper);
		}
	}

	public enum WrapperType {
		SUPERIORSKYBLOCK(SuperiorSkyblockWrapper.class);

		private final Class<? extends Wrapper> wclass;

		WrapperType(Class<? extends Wrapper> wclass) {
			this.wclass = wclass;
		}
	}
}
