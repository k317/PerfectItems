package fr.perfect.items.wrapper.impl;

import com.bgsoftware.superiorskyblock.api.SuperiorSkyblockAPI;
import com.bgsoftware.superiorskyblock.api.wrappers.SuperiorPlayer;
import fr.perfect.items.wrapper.BuildWrapper;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class SuperiorSkyblockWrapper implements BuildWrapper {

	/**
	 * @param location is where the block will be / was
	 * @param player is the source
	 * @return if yes or no this plugin authorize him to build
	 */
	@Override
	public boolean canBuild(Location location, Player player) {
		SuperiorPlayer superiorPlayer = SuperiorSkyblockAPI.getPlayer(player);

		if(player.hasPermission("restriction.bypass"))return true;
		if(!superiorPlayer.hasIsland())return false;
		return superiorPlayer.getIsland().isInside(location);
	}
}
