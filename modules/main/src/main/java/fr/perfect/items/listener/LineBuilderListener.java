package fr.perfect.items.listener;

import de.tr7zw.changeme.nbtapi.NBTItem;
import fr.perfect.items.manager.ItemDataManager;
import fr.perfect.items.manager.UsureManager;
import fr.perfect.items.manager.WrapperManager;
import fr.perfect.items.model.ItemProperty;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import javax.inject.Inject;

import static org.bukkit.Material.AIR;

public class LineBuilderListener implements Listener {

	private final ItemDataManager manager;
	private final UsureManager usure;
	private final WrapperManager wrapper;

	@Inject
	public LineBuilderListener(ItemDataManager manager, UsureManager usure, WrapperManager wrapper) {
		this.manager = manager;
		this.usure = usure;
		this.wrapper = wrapper;
	}

	@EventHandler
	public void onClick(PlayerInteractEvent event) {

		if(event.getAction() != Action.RIGHT_CLICK_BLOCK)return;

		Player player = event.getPlayer();
		Block block = event.getClickedBlock();

		Location blockLocation = block.getLocation();
		ItemStack stack = player.getItemInHand();

		if(stack == null)return;
		if(stack.getType() == AIR)return;

		NBTItem nbtItem = new NBTItem(stack);

		if(!manager.hasItemProperty(nbtItem, ItemProperty.LINEBUILDER))return;
		if(!wrapper.canBuild(player, block.getLocation()))return;

		Material data = manager.getItemProperty(nbtItem, ItemProperty.LINEBUILDER);

		int y = 0;
		while(y <= 256) {
			Block current = blockLocation.clone().add(0, y, 0).getBlock();
			if (current.getType() == null || current.getType() == AIR) {
				current.setType(data);
			}
			y++;
		}

		usure.use(stack, nbtItem, player, 1);
		nbtItem.applyNBT(stack);
	}
}
