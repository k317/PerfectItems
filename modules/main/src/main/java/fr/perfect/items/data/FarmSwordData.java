package fr.perfect.items.data;

public class FarmSwordData implements ItemData {

	private boolean sell;
	private double moneyboost;
	private double expboost;
	private int killer;

	public FarmSwordData() {}

	public FarmSwordData(boolean sell, double money_multiplier, double exp_multiplier, int killer) {
		this.sell = sell;
		this.moneyboost = money_multiplier;
		this.expboost = exp_multiplier;
		this.killer = killer;
	}

	public boolean isSell() {
		return sell;
	}

	public void setSell(boolean sell) {
		this.sell = sell;
	}

	public double getMoneyBoost() {
		return moneyboost;
	}

	public void setMoneyBoost(double moneyboost) {
		this.moneyboost = moneyboost;
	}

	public double getExpBoost() {
		return expboost;
	}

	public void setExpBoost(double expboost) {
		this.expboost = expboost;
	}

	public int getKiller() {
		return killer;
	}

	public void setKiller(int killer) {
		this.killer = killer;
	}
}
