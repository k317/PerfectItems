package fr.perfect.items.manager;

import de.tr7zw.changeme.nbtapi.NBTItem;
import fr.perfect.items.model.ItemProperty;
import fr.perfect.items.utils.LoreUtils;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

@Singleton
@Deprecated
public class UsureManager {

	private final ItemDataManager manager;

	@Inject
	public UsureManager(ItemDataManager manager) {
		this.manager = manager;
	}

	/**
	 * function invoked when a player use his item
	 * check for "itemMeta updater param" such as DURABILITY or STATISTIC
	 *
	 * @param item is the source
	 * @param nbtItem is the source as an NBTItem
	 * @param player is the cause of the event
	 * @param size is the count of use
	 */
	@Deprecated
	public void use(ItemStack item, NBTItem nbtItem, Player player, int size) {
		int durability = reduceDurability(nbtItem, size);
		if(durability == 0) {
			breakItem(item, player);
		}
		updateLore(item, nbtItem, durability, -1);
	}

	/**
	 * break the item in the player hand
	 *
	 * @param stack is the source
	 * @param player is the guy who cause the event
	 */
	private void breakItem(ItemStack stack, Player player) {
		int slot = player.getInventory().first(stack);
		player.getInventory().clear(slot);
	}

	/**
	 * will update the lore of an item with the new properties
	 * will be replaced with a function that care of every "lore updater property"
	 *
	 * @param item is the source
	 * @param nbtItem is the source as an nbtitem
	 * @param durability is the data associated to "DURABILITY" property
	 * @param statistic is the data associated to "STATISTIC" property
	 */
	@Deprecated
	private void updateLore(ItemStack item, NBTItem nbtItem, int durability, int statistic) {

		ItemMeta meta = item.getItemMeta();
		List<String> lore = LoreUtils.readLore(nbtItem);
		lore.replaceAll((line) -> line.replace("%durability%", durability + "")
				.replace("%statistic%", statistic + ""));

		meta.setLore(lore);
		item.setItemMeta(meta);
	}

	/**
	 * will be replaced by an event (PlayerUseItemEvent)
	 *
	 * @param stack is the source as an nbtitem
	 * @param size is the quantity of durability remove
	 * @return the new durability or -1 if no durability property
	 */
	@Deprecated
	public int reduceDurability(NBTItem stack, int size) {
		if(!manager.hasItemProperty(stack, ItemProperty.DURABILITY))return -1;
		int data = manager.getItemProperty(stack, ItemProperty.DURABILITY);
		int result = data-size;
		manager.setItemProperty(stack, ItemProperty.DURABILITY, result);
		return result;
	}
}
