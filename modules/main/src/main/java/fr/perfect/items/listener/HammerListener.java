package fr.perfect.items.listener;

import de.tr7zw.changeme.nbtapi.NBTItem;
import fr.perfect.items.manager.ItemDataManager;
import fr.perfect.items.manager.UsureManager;
import fr.perfect.items.manager.WrapperManager;
import fr.perfect.items.model.ItemProperty;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import javax.inject.Inject;

public class HammerListener implements Listener {

	private final ItemDataManager itemData;
	private final UsureManager usure;
	private final WrapperManager wrapper;

	@Inject
	public HammerListener(ItemDataManager itemData, UsureManager usure, WrapperManager wrapper) {
		this.itemData = itemData;
		this.usure = usure;
		this.wrapper = wrapper;
	}

	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		Block block = event.getBlock();
		ItemStack stack = player.getItemInHand();

		if(stack == null)return;
		if(stack.getType() == Material.AIR)return;

		NBTItem nbtItem = new NBTItem(stack);

		if(!itemData.hasItemProperty(nbtItem, ItemProperty.HAMMER)) return;
		if(!wrapper.canBuild(player, block.getLocation()))return;

		int data = itemData.getItemProperty(nbtItem, ItemProperty.HAMMER);

		int radius = (data - 1) / 2;
		for(int x = -radius; x <= radius; x++)
			for(int z = -radius; z <= radius; z++)
				for(int y = -radius; y <= radius; y++)
					block.getLocation().clone().add(x, y, z).getBlock().breakNaturally(stack);

		usure.use(stack, nbtItem, player, 1);
		nbtItem.applyNBT(stack);
	}
}
