package fr.perfect.items.manager;

import com.google.common.collect.Maps;
import de.tr7zw.changeme.nbtapi.NBTItem;
import fr.perfect.items.model.ItemModel;
import fr.perfect.items.model.ItemProperty;
import fr.perfect.items.reflection.common.VersionManager;
import fr.perfect.items.utils.LoreUtils;
import fr.perfect.reloader.Reloadable;
import fr.perfect.serializer.YamlSerializer;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Singleton
public class ItemModelManager implements Reloadable {

	private final List<ItemModel> models;

	private final ItemDataManager manager;
	private final JavaPlugin plugin;
	private final VersionManager version;
	private final UsureManager usure;

	@Inject
	public ItemModelManager(ItemDataManager manager, UsureManager usure, JavaPlugin plugin, VersionManager version) {
		this.manager = manager;
		this.version = version;
		this.usure = usure;
		this.models = new ArrayList<>();
		this.plugin = plugin;
	}

	/**
	 * Function invoked each time that u want to reload
	 */
	@Override
	public void load() {
		models.clear();
		loadFile(plugin.getConfig());

		File folder = new File(plugin.getDataFolder(), "items");
		if(folder.list() != null)
			for(File content : folder.listFiles())
				loadFile(YamlConfiguration.loadConfiguration(content));
	}

	/**
	 * load a list of item model from a file
	 *
	 * @param configuration is the yaml configuration
	 */
	private void loadFile(FileConfiguration configuration) {
		ConfigurationSection section = configuration.getConfigurationSection("items");
		for(String key : section.getKeys(false)) {
			models.add(load(key, section.getConfigurationSection(key)));
		}
	}

	/**
	 * create a new itemstack with parameters of your itemModel
	 *
	 * @param model is the source
	 * @return the compiled item
	 */
	public ItemStack convert(ItemModel model) {

		ItemStack stack = new ItemStack(model.getMaterial(), model.getAmount());
		ItemMeta meta = stack.getItemMeta();

		if(!model.getDisplayName().isEmpty())meta.setDisplayName(model.getDisplayName());
		if(!model.getFlags().isEmpty())model.getFlags().forEach(meta::addItemFlags);

		model.getEnchantments().forEach((e, i) -> meta.addEnchant(e, i, true));
		stack.setItemMeta(meta);

		if(model.isUnbreakable())version.getItemBuilder().setUnbreakable(stack);
		if(model.getModelID() != 0){
			version.getItemBuilder().setModelID(stack, model.getModelID());
		}

		NBTItem nbtItem = new NBTItem(stack);

		model.getProperties().forEach((p,d) -> manager.setItemProperty(nbtItem, p, d));
		if(!model.getLore().isEmpty()) LoreUtils.storeLore(nbtItem, model.getLore());
		usure.use(stack, nbtItem, null, 0);
		nbtItem.applyNBT(stack);
		return stack;
	}

	/**
	 * @param key is the name of the itemModel (the key of the parent configurationSection)
	 * @param section is where the item parameters will be loaded
	 * @return a new itemModel from config
	 */
	public ItemModel load(String key, ConfigurationSection section) {
		Material material = Material.matchMaterial(section.getString("type", "APPLE"));

		int amount = section.getInt("amount", 1);
		int modelID = section.getInt("modelID", 0);
		String name = section.getString("name", "&cError").replace("&", "§");
		List<String> lore = section.getStringList("lore").stream().map(it -> it.replace("&", "§")).collect(Collectors.toList());
		boolean unbreakable = section.getBoolean("unbreakable", false);
		List<ItemFlag> flags = section.getStringList("flags").stream().map(ItemFlag::valueOf).collect(Collectors.toList());
		Map<Enchantment, Integer> enchantments = loadEnchantments(section);
		Map<ItemProperty, Object> properties = loadProperties(section);
		return new ItemModel(key, amount, modelID, material, name, lore, enchantments, flags, properties, unbreakable);
	}

	/**
	 * will load enchant from config section with as key the enchant name and as value the enchant power + 1
	 *
	 * @param section the configuration section where the enchant is
	 * @return the map of enchantment to integer
	 */
	private Map<Enchantment, Integer> loadEnchantments(ConfigurationSection section) {
		ConfigurationSection configSection = section.getConfigurationSection("enchantments");
		Map<Enchantment, Integer> enchantments = Maps.newHashMap();
		if(configSection == null)return enchantments;
		for(String e : configSection.getKeys(false)) {
			enchantments.put(Enchantment.getByName(e.toUpperCase()), configSection.getInt(e)-1);
		}
		return enchantments;
	}

	/**
	 * load special property from config
	 *
	 * @param section is the source
	 * @return the map of itemproperty associated to their data class
	 */
	private Map<ItemProperty, Object> loadProperties(ConfigurationSection section) {
		Map<ItemProperty, Object> properties = Maps.newHashMap();
		ConfigurationSection propertiesSection = section.getConfigurationSection("properties");
		if(propertiesSection == null)return properties;
		for(String key2 : propertiesSection.getKeys(false)) {
			ItemProperty current = ItemProperty.valueOf(key2.toUpperCase());
			ConfigurationSection propertySection = section.getConfigurationSection("properties." + key2);
			Object data = null;
			if(current.getTclass() != null) data = YamlSerializer.deserialize(propertySection, current.getTclass());
			properties.put(current, data);
		}
		return properties;
	}

	/**
	 * @return itemModel cache list
	 */
	public List<ItemModel> getModels() {
		return models;
	}

	/**
 	 * @param token is the key source
	 * @return the first model with this key stored in the cache
	 */
	public Optional<ItemModel> getBy(String token) {
		return models.stream().filter(m -> m.getToken().equalsIgnoreCase(token)).findFirst();
	}
}
