package fr.perfect.items.reflection.common;

public interface VersionManager {

	/**
	 * @return the item utils class instance
	 */
	ItemBuilder getItemBuilder();
}
