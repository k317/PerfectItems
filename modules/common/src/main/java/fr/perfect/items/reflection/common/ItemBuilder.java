package fr.perfect.items.reflection.common;

import org.bukkit.inventory.ItemStack;

public interface ItemBuilder {

	/**
	 * @param stack is the source
	 */
	void setUnbreakable(ItemStack stack);

	/**
	 * @param stack is the source
	 * @param modelID is the customModelData
	 */
	void setModelID(ItemStack stack, int modelID);
}
